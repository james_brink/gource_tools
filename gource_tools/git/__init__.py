# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
import re
from gource_tools import err


class Repo(object):
    """
    Git repo.
    """
    def __init__(self, name, directory, verbose=False):
        self.name = name
        self.directory = directory
        self.report_errors = verbose
        self.head = None
        self.branches = []
        self.tags = []
        self.heads = []
        self.files = ()
        self.logs = []
        self.__get_refs()
        self.set_head()

    def set_head(self, ref='HEAD'):
        self.files = ()
        self.head = ref
        self.__collect_logs()

    def __add_file(self, file):
        all_files = self.files + (file,)
        self.files = tuple(set(all_files))

    def __get_refs(self):
        """
        Collect all git refs
        """
        git_command = ['/usr/bin/env', 'git', '--git-dir', self.directory,
                       'for-each-ref', '--format=%(refname:lstrip=-2)']
        proc = Popen(git_command, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = proc.communicate()
        tag_pattern = re.compile('^tags/.+')
        heads_pattern = re.compile('^heads/.+')
        if proc.returncode == 0:
            result = stdout.decode('utf-8')
            for line in result.splitlines():
                if re.match(tag_pattern, line):
                    self.tags.append(line.replace('tags/', ''))
                elif re.match(heads_pattern, line):
                    self.heads.append(line.replace('heads/', ''))
                else:
                    self.branches.append(line)
        else:
            if self.report_errors:
                err(stderr.decode('utf-8'))

    def __collect_logs(self):
        """
        Read current gitref log.
        """
        git_log_format = '--pretty=format:user:%aN%n%ct'
        git_command = ['/usr/bin/env', 'git', '--no-pager', '--git-dir',
                       self.directory, 'log', git_log_format, '--reverse',
                       '--raw', '--encoding=UTF-8', '--no-renames', self.head]
        proc = Popen(git_command, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = proc.communicate()
        commit_pattern = re.compile('^$\n', re.MULTILINE)
        while proc.returncode is None:
            proc.poll()
        if proc.returncode == 0:
            output = stdout.decode('utf-8')
            raw_logs = re.split(commit_pattern, output)
            self.logs = self.__parse_commits(raw_logs)
        else:
            if self.report_errors:
                err('Could not read log for repo: ' + self.name + ' - ref: ' + self.head)

    def __parse_commits(self, logs):
        """
        Parse commit messages from logs.
        """
        parsed_commits = []
        user_date_pattern = re.compile('^user:(.+)$\n^(\d+)$\n', re.MULTILINE)
        file_changes_pattern = re.compile('^:([A-Fa-f0-9]{6,7}\s+){4}(A|M|D)\s+(.+)$', re.MULTILINE)
        for commit in logs:
            parsed_commit = {}
            user_date = user_date_pattern.match(commit)
            file_changes = file_changes_pattern.findall(commit)
            if user_date and file_changes:
                parsed_commit['user'] = user_date.group(1)
                parsed_commit['date'] = user_date.group(2)
                parsed_commit['files'] = []
                for file in file_changes:
                    operation = file[1]
                    file_name = file[2]
                    file_change = {'operation': operation, 'name': file_name}
                    parsed_commit['files'].append(file_change)
                    self.__add_file(file_name)
                parsed_commits.append(parsed_commit)
        return parsed_commits
