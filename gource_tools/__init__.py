# -*- coding: utf-8 -*-
import sys
from colorama import init, Fore, Style

# Call init for colorama
init()
"""
Top-level package for Gource Tools.
"""
__author__ = """James Brink"""
__email__ = 'brink.james@gmail.com'
__version__ = '0.1.0'


def out(message):
    """
    print alias
    """
    sys.stdout.write(message + "\n" + Style.RESET_ALL)
    sys.stdout.flush()


def err(message):
    """
    print alias for errors
    """
    error_message = Fore.RED + message + Style.RESET_ALL
    out(error_message)
