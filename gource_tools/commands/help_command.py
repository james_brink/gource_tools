# -*- coding: utf-8 -*-
from gource_tools.commands import usage


def add_arguments(parser):
    """
    adds arguments for the help command
    """
    usage()
    exit(0)


def execute(args):
    """
    empty command to allow help messages to work
    """
    pass
