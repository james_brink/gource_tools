# -*- coding: utf-8 -*-
from gource_tools import out
import os
import sys

COMMAND_MODULE_PATH = os.path.dirname(__file__)
COMMAND_MODULE_SUFFIX = '_command.py'


def usage():
    commands = get_command_names()
    out('usage: ')
    for command in commands:
        out('\t' + command)


def get_command_names():
    """
    Returns a list of command names.
    """
    names = []
    for f in os.listdir(COMMAND_MODULE_PATH):
        if os.path.isfile(os.path.join(COMMAND_MODULE_PATH, f)) and f.endswith(COMMAND_MODULE_SUFFIX):
            names.append(f[:-len(COMMAND_MODULE_SUFFIX)])
    return names


def get_command(name):
    """
    Returns a command module
    """
    __import__('gource_tools.commands.' + name + '_command')
    return sys.modules['gource_tools.commands.' + name + '_command']
