# -*- coding: utf-8 -*-
import os
from subprocess import PIPE, Popen
import operator
from gource_tools import err, out
from gource_tools.git import Repo
import random
import re


def add_arguments(parser):
    """
    Adds arguments for the collect command.
    """
    parser.add_argument('-d', '--repo_dir', help='Directory containing a git repository.')
    parser.add_argument('-b', '--base_dir', help='Base directory with git repositorites (For collecting multiple repos).')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output.')
    parser.add_argument('-o', '--output_file', help='Output file for log history.')


def execute(args):
    """
    Executes the collect command.
    """
    repos = get_repos(args)
    all_logs = []
    for repo in repos:
        set_file_root(repo, repo.name)
        for commit in repo.logs:
            all_logs.append(commit)
        # branches = repo.branches
        # for branch in branches:
        #     repo.set_head(branch)
        #     branch_name = re.sub("origin/", "", repo.head)
        #     branch_commit = str(len(repo.logs))
        #     branch_files = str(len(repo.files))
        #     fmt = '{:<10}({}/{}){:>12}{:<10}{:<15}{:<10}'
        #     print("=" * 80)
        #     print(fmt.format("Branch:", repo.name, branch_name, "Commits:", branch_commit, "Files:", branch_files))
        #     print("=" * 80)
        #     get_user_stats(repo.logs)
    sorted_logs = sort_logs(all_logs)
    print_logs(sorted_logs)
            


def get_user_stats(log):
    stats = {}
    for commit in log:
        user = commit['user']
        file_count = len(commit['files'])
        if user in stats:
            stats[user]['commits'] += 1
            stats[user]['files'] += file_count
        else:
            stats[user] = {'commits': 1, 'files': file_count}
    sorted_stats = sorted(stats, key=lambda k: stats[k]['commits'])
    for user in sorted_stats:
        fmt = '{:>40}{:>10}{:>5}{:>10}{:>5}'
        print(fmt.format(user, 'commits:', stats[user]['commits'], 'files:', stats[user]['files']))
    return stats


def set_file_root(repo, new_root):
    """
    Changes the file root for every commit.
    """
    new_logs = []
    for commit in repo.logs:
        new_commit = commit
        new_files = []
        for file in commit['files']:
            new_file = file
            new_file['name'] = os.path.join(new_root, file['name'])
            new_files.append(new_file)
        new_commit['files'] = new_files
        new_logs.append(new_commit)
    repo.logs = new_logs


def get_repos(args):
    """
    Returns a list of Repo objects
    """
    repos = []
    if args.base_dir:
        repos = collect_repos(args.base_dir, args.verbose)
    elif args.repo_dir:
        repo = inspect_repo(args.repo_dir)
        if not repo:
            err('No git repository found at: ' + args.repo_dir)
            exit(1)
        else:
            repos.append(repo)
    return repos


def sort_logs(logs):
    """
    sorts logs based on date.
    """
    return sorted(logs, key=lambda k: k['date'])


def print_logs(logs):
    """
    prints log output for use with gource.
    """
    for commit in logs:
        fmt = '{}|{}|{}|/{}'
        for file in commit['files']:
            output = fmt.format(commit['date'], commit['user'],
                                file['operation'], file['name'])
            out(output)


def inspect_repo(dir, verbose=False):
    """
    Inspects a directory to consume git information.
    Returns a Repo object.
    """
    repo = None
    fmt = '{:<12}{:<40}{:<10}{:<10}{:<6}{:<8}{:<10}{:<8}'
    git_dir = os.path.join(dir) + '/.git'
    git_command = ['/usr/bin/env', 'git', '--git-dir', git_dir, 'status']
    proc = Popen(git_command, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = proc.communicate()
    if proc.poll() == 0:
        repo_name = os.path.basename(dir)
        repo = Repo(repo_name, git_dir, verbose)
        if verbose:
            out(fmt.format('Adding:', repo.name, 'branches:',
                           len(repo.branches), 'tags:', len(repo.tags),
                           'commits:', len(repo.logs)))
    else:
        if verbose:
            err(fmt.format('Skipping:', dir, 'N/A', 'N/A',
                           'N/A', 'N/A', 'N/A', 'N/A'))
    return repo


def collect_repos(base_dir, verbose=False):
    """
    Searches a base_directory and looks for git repos.
    Returns a list of Repo objects.
    """
    dirs = next(os.walk(base_dir))[1]
    valid_repos = []
    for dir in dirs:
        full_path = os.path.join(base_dir, dir)
        repo = inspect_repo(full_path, verbose)
        if repo:
            valid_repos.append(repo)
    return valid_repos
