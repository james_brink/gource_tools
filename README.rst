============
Gource Tools
============


.. image:: https://img.shields.io/pypi/v/gource_tools.svg
        :target: https://pypi.python.org/pypi/gource_tools

.. image:: https://img.shields.io/travis/jamesbrink/gource_tools.svg
        :target: https://travis-ci.org/jamesbrink/gource_tools

.. image:: https://readthedocs.org/projects/gource-tools/badge/?version=latest
        :target: https://gource-tools.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Collection of tools to be used with Gource.


* Free software: MIT license
* Documentation: https://gource-tools.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
